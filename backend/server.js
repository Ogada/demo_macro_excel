const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const exceljs = require('exceljs');
const path = require('path');
const multer = require('multer'); // Import Multer middleware

const app = express();
const port = 3000;

// Use body-parser middleware to parse request bodies
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Serve static files from the frontend directory
app.use(express.static(path.join(__dirname, '../frontend')));

// Configure Multer middleware for file uploads
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        // Set the destination directory for uploaded files
        cb(null, path.join(__dirname, 'uploads'));
    },
    filename: (req, file, cb) => {
        // Use the original file name for uploaded files
        cb(null, file.originalname);
    }
});
const upload = multer({ storage: storage });

// Endpoint to handle file upload using Multer
app.post('/upload', upload.single('file'), (req, res) => {
    // Check if a file was uploaded
    if (!req.file) {
        return res.status(400).send('No file was uploaded.');
    }

    // Process the uploaded file if needed(include this later)

    // Send a response with the download URL of the uploaded file
    res.send({
        downloadUrl: `/downloads/${req.file.filename}`
    });
});

// Endpoint to execute VBA macro
app.post('/executeMacro', (req, res) => {
    // Extract macro data from the request body
    const { displayName, name, description, type, code } = req.body;

    // Load the input Excel file
    const workbook = new exceljs.Workbook();
    workbook.xlsx.readFile(path.join(__dirname, 'uploads', 'input.xlsx')).then(() => {
        const worksheet = workbook.getWorksheet('Sheet1');

        // Execute VBA macro code or use exceljs to process the Excel file
        workbook.xlsx.writeFile(path.join(__dirname, 'uploads', 'output.xlsx')).then(() => {
            // Send a success response if the macro execution is successful
            res.send('Macro executed successfully');
        }).catch(error => {
            // Send an error response if there's an error executing the macro
            res.status(500).send('Error executing macro');
        });
    }).catch(error => {
        // Send an error response if there's an error reading the input file
        res.status(500).send('Error reading input file');
    });
});

// Endpoint to serve the homepage
app.get('/', (req, res) => {
    // Log a message when the homepage is requested
    console.log('Homepage requested');
    // Serve the index.html file
    res.sendFile(path.join(__dirname, '../frontend', 'index.html'));
});

// Start the server and listen on the specified port
app.listen(port, () => {
    // Log a message when the server starts running
    console.log(`Server running at http://localhost:${port}`);
});
