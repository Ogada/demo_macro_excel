// Function to handle file upload
async function uploadFile() {
    // Get the file input element
    const fileInput = document.getElementById('fileInput');
    // Get the selected file
    const file = fileInput.files[0];
    // Check if a file is selected
    if (!file) {
        console.log('No file selected'); // Log a message when no file is selected
        alert('No file selected, Please select a file.'); // Show an alert to prompt the user to select a file
        return;
    }

    // Create a new FormData object to store the file
    const formData = new FormData();
    formData.append('file', file);

    try {
        console.log('Sending upload request...'); // Log a message before sending the upload request
        // Send a POST request to the '/upload' endpoint with the file data
        const response = await sendRequest('/upload', 'POST', formData);
        console.log('Upload request successful'); // Log a message after successful upload
        // Parse the response JSON data
        const data = await response.json();

        // Display a link to download the uploaded file
        const outputDiv = document.getElementById('output');
        outputDiv.innerHTML = `<a href="${data.downloadUrl}" download>Anonymized Document</a>`;
    } catch (error) {
        console.error('Error:', error); // Log any errors that occur during the upload process
        alert('An error occurred. Please try again.'); // Show an alert to inform the user about the error
    }
}

// Function to send HTTP request
async function sendRequest(url, method, body) {
    try {
        const response = await fetch(url, {
            method: method,
            body: body
        });
        return response;
    } catch (error) {
        throw new Error('Error sending request');
    }
}

// Function to execute VBA macro
async function executeMacro() {
    const code = `
        // VBA macro code
        Public Sub order_claims()
        allMacros.Add(new Macro(displayName, name, description, type, code));

            Application.ScreenUpdating = False

            ' Global variables
            Dim i As Integer
            Dim j As Integer
            Const rows As Integer = 100
            Dim Claims(1 To rows) As String

            ' Fill the Claims array in order from column H
            For i = 1 To rows
                Claims(i) = Cells(i, 10).Value
            Next i
   
            ' Loop all files from column A
            For i = 2 To rows
                file = Cells(i, 1).Value
    
                'Search the file in the claims array and write the index order in column I
                For j = 1 To rows
                    claim = Claims(j)
       
                    If (Len(claim) > 2 And InStr(file, claim)) Then
                        Cells(i, 10).Value = j
                    End If
        
                Next j
    
            Next i
    
            'Order the rows by the order index in column J
            Columns("A:J").Select
            ActiveWorkbook.Worksheets("Sheet1").Sort.SortFields.Clear
            ActiveWorkbook.Worksheets("Sheet1").Sort.SortFields.Add2 Key:=Range("J2:J100") _
                , SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
            With ActiveWorkbook.Worksheets("Sheet1").Sort
                .SetRange Range("A1:J100")
                .Header = xlYes
                .MatchCase = False
                .Orientation = xlTopToBottom
                .SortMethod = xlPinYin
                .Apply
            End With
            Range("A1").Select
    
            Columns("J:N").Select
            Selection.Delete Shift:=xlToLeft
    
            Application.ScreenUpdating = True

            ' Save the final file
            Application.DisplayAlerts = wdAlertsNone`;

    try {
        console.log('Sending macro execution request...'); // Log a message before sending the macro execution request
        // Send a POST request to the '/executeMacro' endpoint with macro data
        const response = await sendRequest('/executeMacro', 'POST', { code: code });
        console.log('Macro execution request successful'); // Log a message after successful macro execution
        // Parse the response JSON data
        const data = await response.json();

        // Handle response data if needed
    } catch (error) {
        console.error('Error:', error); // Log any errors that occur during the macro execution process
        alert('An error occurred. Please try again.'); // Show an alert to inform the user about the error
    }
}
